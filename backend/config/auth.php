<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => env('AUTH_GUARD', 'api'),
    ],

    /*
|--------------------------------------------------------------------------
| Authentication Guards
|--------------------------------------------------------------------------
| ........
|
*/

'guards' => [
  /*
  'api' => [
        'driver' => 'jwt',
        'provider' => 'users'
    ],
  'custom' => ['driver' => 'custom'],
    */
    'api' => ['driver' => 'api'],
],

/*
|--------------------------------------------------------------------------
| User Providers
|--------------------------------------------------------------------------
| ..............
|
*/

'providers' => [
    'users' => [
        'driver' => 'eloquent',
        'model'  => App\Models\User::class,
    ]
],

    'passwords' => [
        //
    ],

];
