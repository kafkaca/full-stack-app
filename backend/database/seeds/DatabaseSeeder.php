<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\User;
class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		      Model::unguard();
          //$this->call(UsersTableSeeder::class);

          factory(User::class)->create([
              'username' => 'superuser',
              'email' => 'superuser@gmail.com',
              'password' => sha1("superuser123456")
              //'password' => app('hash')->make('123456')
          ]);

       /*   factory(User::class)->create([
              'username' => 'user',
              'email' => 'user@example.com',
              'password' => sha1("123456")
            //  'password' => app('hash')->make('123456')
          ]);*/
        Model::reguard();


    }
}
