<?php
//https://laravel.com/docs/5.4/helpers


function convert($size)
{
  $unit=array('b','kb','mb','gb','tb','pb');
  return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function getMemory()
{
  return convert(memory_get_usage(true));
}

function is_serial($string) {
  return (@unserialize($string) !== false || $string == 'b:0;');
}

function getUserInfo() {
  $user_id = $_COOKIE['user_id'];
  $user_name = $_COOKIE['user_name'];
  $info_array = [
    'user_id' => $user_id,
    'user_name' => $user_name
];
  return $info_array;
}


function bannerUpload($file){
  $path = base_path('public/uploads/images/');
  $fileName = base64_encode($file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
  $dosyaadi = $path . $fileName;
  if (file_exists($dosyaadi)) {
    unlink($dosyaadi);
  }
  $file->move($path, $fileName);
  return $fileName;
}

function rulesToArrayConvert($rules_array){
  $return_rules = [];
  foreach ($rules_array as $key => $value) {
    switch ($value->meta_name) {
      case "browsers":
      $return_rules[$value->meta_name] = explode(",", $value->meta_value);
      break;
      case "lokasyon":
      $return_rules[$value->meta_name] = explode(",", $value->meta_value);
      break;
      case "diller":
      $return_rules[$value->meta_name] = explode(",", $value->meta_value);
      break;
      case "networks":
      $return_rules[$value->meta_name] = explode(",", $value->meta_value);
      break;
      case "platforms":
        $return_rules[$value->meta_name] = explode(",", $value->meta_value);
        break;
        default :
        $return_rules[$value->meta_name] = $value->meta_value;
      }
    }
    return $return_rules;
}
function rulesToStringConvert($rules_array){
  $demo_arr = [
    "camp_id" => $rules_array["camp_id"]
  ];
  $convert_array = [];
  foreach ($rules_array["form_data"] as $key => $value) {
    $demo_arr["meta_name"] = $key;
    if(is_array($value)){
      $demo_arr["meta_value"] = implode(",", $value);
    } else{
      $demo_arr["meta_value"] = $value;
    }
    array_push($convert_array, $demo_arr);
  }
  return $convert_array;
}

if (!function_exists('storage_path')) {
  function storage_path($path = '')
  {
    return app()->basePath().'/storage'.($path ? '/'.$path : $path);
  }
}
if ( ! function_exists('config_path'))
{
  function config_path($path = '')
  {
    return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
  }
}
