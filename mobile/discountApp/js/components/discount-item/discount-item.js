import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  H3,
  Button,
  Icon,
  Footer,
  FooterTab,
  Left,
  Right,
  Body
} from "native-base";
import staticData from "../../staticData"
import styles from "./styles";

class DiscountItem extends Component {
      constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
            isLoading: true
        }
    }

  render() {
    const {state} = this.props.navigation;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="ios-menu" />
            </Button>
          </Left>
          <Body>
            <Title>Discount Id: {state.params.data.id}</Title>
          </Body>
          <Right />

        </Header>

        <Content padder>
          <Text>
            {state.params.data.title}
          </Text>

        </Content>

        <Footer>
          <FooterTab>
            <Button active full>
              <Text onPress={() => this.props.navigation.navigate('DiscountsList')}>Go Back</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default DiscountItem;
