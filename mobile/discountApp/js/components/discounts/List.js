import React, { Component } from "react";
import { Image, View, ActivityIndicator, ListView } from "react-native";

import {
    Container,
    Header,
    Title,
    List,
    ListItem,
    Spinner,
    Content,
    Button,
    Icon,
    Card,
    CardItem,
    Text,
    Thumbnail,
    Left,
    Body,
    Right,
    IconNB
} from "native-base";

import styles from "./styles";

const logo = require("../../../img/logo.png");
const cardImage = require("../../../img/drawer-cover.png");
import DiscountItem from "../discount-item/discount-item"
class DiscountList extends Component {

    constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
            isLoading: true,
            dataSource: [],
            clickToData: {},
            clickToView: false
        }
    }
    componentDidMount() {
        return fetch('http://hoppi.liberyen.com/api/list/discounts')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson.data
                }, function () {
                    // do something with new state
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    onClickItem(data) {
        this.setState({
            clickToView: true,
            clickToData: data
        }, function () {
            // do something with new state
        });
    }
    onClickToView() {
        this.setState({
            clickToView: false
        }, function () {
            // do something with new state
        });
    }
    render() {
        const {navigate} = this.props.navigation;

        if (this.state.clickToView) {
            return (
                <DiscountItem data={this.state.clickToData} actionClick={() => this.onClickToView.bind(this)} />
            );
        }
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("DrawerOpen")}
                        >
                            <Icon name="menu" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Discounts</Title>
                    </Body>
                    <Right />

                </Header>

                <Content padder>

                    {this.state.isLoading === false ? <List
                        style={{ margin: 5 }}
                        dataArray={this.state.dataSource}
                        renderRow={discount =>
                            <Card style={styles.mb}>
                                <CardItem>
                                    <Left>
                                        <Thumbnail source={logo} />
                                        <Body>
                                            <Text>{discount.title}</Text>
                                            <Text note>{discount.id * 10} £</Text>
                                        </Body>
                                    </Left>
                                </CardItem>

                                <CardItem cardBody>
                                    <Image
                                        style={{
                                            resizeMode: "cover",
                                            width: null,
                                            height: 200,
                                            flex: 1
                                        }}
                                        source={{ uri: "http://loremflickr.com/320/240/street?random=" + discount.id }}
                                    />
                                    <Text style={{ position: "absolute", top: 50, left: 50, color: "#fff" }}>Card Resmi {discount.id}</Text>
                                </CardItem>

                                <CardItem style={{ paddingVertical: 0 }}>
                                    <Left>
                                        <Button iconLeft transparent
                                            onPress={() => navigate('DiscountItem', { data: discount })}>
                                            <Icon active name="eye" style={{ justifyContent: "center", color: "red" }} />
                                            <Text> View </Text>
                                        </Button>
                                    </Left>
                                    <Body>
                                        <Button iconLeft transparent>
                                            <Icon active name="chatbubbles" />
                                            <Text>89 Comments</Text>
                                        </Button>
                                    </Body>
                                    <Right>
                                        <Text>11h ago</Text>
                                    </Right>
                                </CardItem>
                            </Card>
                        } /> : <View style={{ flex: 1, paddingTop: 100 }}>
                            <Spinner color="blue" />
                        </View>}




                </Content>
            </Container>
        );
    }
}

export default DiscountList;
