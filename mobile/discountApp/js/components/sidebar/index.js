import React, { Component } from "react";
import { Image } from "react-native";

import {
	Content,
	Text,
	List,
	ListItem,
	Icon,
	Container,
	Left,
	Right,
	Badge,
	Button,
	View,
	StyleProvider,
	getTheme,
	variables,
} from "native-base";

import styles from "./style";

const drawerCover = require("../../../img/drawer-cover.png");

const drawerImage = require("../../../img/logo.png");
const datas = [
	{
		name: "Kampanyalar",
		route: "Anatomy",
		icon: "phone-portrait",
		bg: "#C5F442",
	},
	{
		name: "Filtreler",
		route: "Actionsheet",
		icon: "easel",
		bg: "#C5F442",
	},
	{
		name: "Detaylı Ara",
		route: "Header",
		icon: "phone-portrait",
		bg: "#477EEA",
		types: "8",
	},
	{
		name: "Ayarlar",
		route: "Footer",
		icon: "phone-portrait",
		bg: "#DA4437",
		types: "4",
	},
	{
		name: "Bildirimler",
		route: "NHBadge",
		icon: "notifications",
		bg: "#4DCAE0",
	},
	{
		name: "Radar",
		route: "NHButton",
		icon: "radio-button-off",
		bg: "#1EBC7C",
		types: "9",
	},
	{
		name: "Listeler",
		route: "NHCard",
		icon: "keypad",
		bg: "#B89EF5",
		types: "5",
	},
	{
		name: "Discounts",
		route: "DiscountsList",
		icon: "keypad",
		bg: "#B89EF5",
		types: "5",
	},
	{
		name: "Add Discount",
		route: "AddDiscount",
		icon: "keypad",
		bg: "#B89EF5",
		types: "5",
	}
];

class SideBar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			shadowOffsetWidth: 1,
			shadowRadius: 4,
		};
	}

	render() {
		return (
			<Container>
				<Content bounces={false} style={{ flex: 1, backgroundColor: "#fff", top: -1 }}>
					<Image source={drawerCover} style={styles.drawerCover}>
						<Image square style={styles.drawerImage} source={drawerImage} />
					</Image>
					<List
						dataArray={datas}
						renderRow={data =>
							<ListItem button noBorder onPress={() => this.props.navigation.navigate(data.route)}>
								<Left>
									<Icon active name={data.icon} style={{ color: "#777", fontSize: 26, width: 30 }} />
									<Text style={styles.text}>
										{data.name}
									</Text>
								</Left>
								{data.types &&
									<Right style={{ flex: 1 }}>
										<Badge
											style={{
												borderRadius: 3,
												height: 25,
												width: 72,
												backgroundColor: data.bg,
											}}
										>
											<Text style={styles.badgeText}>{`${data.types} Item`}</Text>
										</Badge>
									</Right>}
							</ListItem>}
					/>
				</Content>
			</Container>
		);
	}
}

export default SideBar;
