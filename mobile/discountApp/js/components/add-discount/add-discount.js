import React, { Component } from "react";
//import ImagePicker from "react-native-image-picker";
import { Image, View, ActivityIndicator, ListView } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Label,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text
} from "native-base";

import styles from "./styles";

class AddDiscount extends Component {

    constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
            isLoading: true,
            avatarSource: {}
        }
    }
testUpload(){
var ImagePicker = require('react-native-image-picker');

var storageOptions = {
    skipBackup: true,
    path: 'images'
  };

ImagePicker.launchImageLibrary(storageOptions, (response)  => {

   // let source = { uri: response.uri, isStatic: true, height: 200, width: 200};
  let source = { uri: response.uri};
  console.log(response);
  console.log(this.state.avatarSource);
  console.log(source);
    this.setState({
      avatarSource: source
    });
      console.log(JSON.stringify(this.state.avatarSource));
});

 
}

    _uploadSnap() {
  var url = 'http://example.com/upload'; // File upload web service path
  var photo = {
      uri: 'this.state.picturePath', // CameralRoll Url
      type: 'image/jpeg',
      name: 'photo.jpg',
  };

  var formData = new FormData();
  formData.append("file", photo);

  var xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  console.log('OPENED', xhr.status);

  xhr.onprogress = function () {
      console.log('LOADING', xhr.status);
  };

  xhr.onload = function () {
      console.log('DONE', xhr.status);
  };
  
  xhr.setRequestHeader('authorization', this.state.token);
  xhr.send(formData);
}


  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Add Discount</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Title</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Content</Label>
              <Input />
            </Item>
            <Item floatingLabel last>
              <Label>Price</Label>
              <Input />
            </Item>
          </Form>
          <Button block style={{ margin: 15, marginTop: 50 }}>
            <Text>Submit</Text>
          </Button>
           <Button block style={{ margin: 15, marginTop: 50 }}  onPress={this.testUpload.bind(this)}>
            <Text>Select Photo</Text>
          </Button>
          <Image source={this.state.avatarSource} style={{width: null,
                                            height: 200,
                                            flex: 1
                                        }} />
        </Content>
      </Container>
    );
  }
}

export default AddDiscount;
