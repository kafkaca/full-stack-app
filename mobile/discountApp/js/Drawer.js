/* @flow */

import React from "react";
import { DrawerNavigator } from "react-navigation";

import Home from "./components/home/";
import SideBar from "./components/sidebar";
import NHCard from "./components/card/";
import DiscountsList from "./components/discounts/List";
import DiscountItem from "./components/discount-item/discount-item";
import AddDiscount from "./components/add-discount/add-discount"
const DrawerExample = DrawerNavigator(
  {
    Home: { screen: Home },
    NHCard: { screen: NHCard },
    DiscountsList : {screen : DiscountsList},
    DiscountItem : {screen : DiscountItem},
    AddDiscount : {screen : AddDiscount}
  },
  {
    initialRouteName: "Home",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);

export default DrawerExample;
