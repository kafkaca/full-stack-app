import React, { Component } from "react";
import { ListView, Image, View } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Text,
  Thumbnail,
  Left,
  Body,
  Right,
  IconNB
} from "native-base";

import styles from "./styles";

const logo = require("../img/logo.png");
const cardImage = require("../img/drawer-cover.png");

class NHCardImage extends Component {
    constructor(props) {
    super(props);
  }

  render() {
    return (
        <Content padder>
        <ListView
          dataSource={this.props.datalar}
          renderRow={(rowData) =>
            <Card style={styles.mb}>
              <CardItem>
                <Left>
                  <Thumbnail source={logo} />
                  <Body>
                    <Text>{rowData.title}</Text>
                    <Text note>GeekyAnts</Text>
                  </Body>
                </Left>
              </CardItem>

              <CardItem cardBody>
                <Image
                  style={{
                    resizeMode: "cover",
                    width: null,
                    height: 200,
                    flex: 1
                  }}
                  source={{uri : 'http://loremflickr.com/320/240/paris,girl?random=' + rowData.id}}
                />
                <Text style={{ position: "absolute", top: 50, left: 50, color: "#fff" }}>Card Resmi</Text>
              </CardItem>

              <CardItem style={{ paddingVertical: 0 }}>
                <Left>
                  <Button iconLeft transparent>
                    <Icon active name="thumbs-up" />
                    <Text>4923 Likes</Text>
                  </Button>
                </Left>
                <Body>
                  <Button iconLeft transparent>
                    <Icon active name="chatbubbles" />
                    <Text>89 Comments</Text>
                  </Button>
                </Body>
                <Right>
                  <Text>11h ago</Text>
                </Right>
              </CardItem>
            </Card>
          }
        />
        </Content>
    );
  }
}

export default NHCardImage;
